#!/bin/bash

File=$1

GREEN="\033[92m"
RS="\033[0m"

while IFS='@' read -r country region postalzip name address currency
do

if [[ -n postalzip ]]
then 

mkdir -p "result"/"$country"/"$region"/ 
cat > "result"/"$country"/"$region"/"$postalzip".yaml <<EOT
name: ${name}
address: ${address}
curr: ${currency}
EOT

printf "${GREEN}$postalzip.yaml successfully created${RS}\n"

fi

done < <(awk -v FPAT='"[^"]*"|[^,]*' -v OFS='@' 'NR!=1 {print $1, $2, $3, $4, $5, $6}' $File)

tar -cjf result.tar.xz result